package factory;

import configurations.PropManager;
import utils.entities.authors.Author;
import utils.entities.books.Book;
import utils.entities.genres.Genre;

import javax.xml.soap.*;

import static constants.SoapRequestElements.*;
import static constants.SoapRequestElements.PUBLICATION_YEAR;

public class Factory {
    public static SOAPConnection createConnection()  {
        SOAPConnection soapConnection = null;
        try {
            soapConnection = SOAPConnectionFactory.newInstance().createConnection();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return soapConnection;
    }

    public static SOAPMessage createSoapMessage() {
        SOAPMessage soapMessage = null;
        try {
            soapMessage = MessageFactory.newInstance().createMessage();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return soapMessage;
    }

    public static  SOAPElement createAuthor(SOAPElement element, Author author) throws SOAPException {
        SOAPElement authorElement = element.addChildElement("author", PropManager.getLibName());
        SOAPElement authorID = authorElement.addChildElement("authorId", PropManager.getLibName());
        authorID.addTextNode(author.getAuthorId().toString());
        SOAPElement authorName = authorElement.addChildElement("authorName", PropManager.getLibName());
        SOAPElement firstName = authorName.addChildElement("first", PropManager.getLibName());
        firstName.addTextNode(author.getAuthorName().getFirst());
        SOAPElement secondName = authorName.addChildElement("second", PropManager.getLibName());
        secondName.addTextNode(author.getAuthorName().getSecond());
        SOAPElement nationality = authorElement.addChildElement("nationality", PropManager.getLibName());
        nationality.addTextNode(author.getNationality());
        SOAPElement birth = authorElement.addChildElement("birth", PropManager.getLibName());
        SOAPElement date = birth.addChildElement("date", PropManager.getLibName());
        date.addTextNode(author.getBirth().getDate());
        SOAPElement country = birth.addChildElement("country", PropManager.getLibName());
        country.addTextNode(author.getBirth().getCountry());
        SOAPElement city = birth.addChildElement("city", PropManager.getLibName());
        city.addTextNode(author.getBirth().getCity());
        SOAPElement authorDescription = authorElement.addChildElement("authorDescription", PropManager.getLibName());
        authorDescription.addTextNode(author.getAuthorDescription());
        return authorElement;
    }

    public static SOAPElement createBook(SOAPElement element, Book book) throws SOAPException {
        SOAPElement bookEl = element.addChildElement(BOOK,PropManager.getLibName());
        SOAPElement bookId = bookEl.addChildElement(BOOK_ID, PropManager.getLibName());
        bookId.addTextNode(book.getBookId().toString());
        SOAPElement bookName = bookEl.addChildElement(BOOK_NAME, PropManager.getLibName());
        bookName.addTextNode(book.getBookName());
        SOAPElement bookLanguage = bookEl.addChildElement(BOOK_LANGUAGE, PropManager.getLibName());
        bookLanguage.addTextNode(book.getBookLanguage());
        SOAPElement bookDescription = bookEl.addChildElement(BOOK_DESCRIPTION, PropManager.getLibName());
        bookDescription.addTextNode(book.getBookDescription());

        SOAPElement additional = bookEl.addChildElement(ADDITIONAL, PropManager.getLibName());
        SOAPElement pageCount = additional.addChildElement(PAGE_COUNT, PropManager.getLibName());
        pageCount.addTextNode(book.getAdditional().getPageCount().toString());

        SOAPElement size = additional.addChildElement(SIZE, PropManager.getLibName());
        SOAPElement height = size.addChildElement(HEIGHT, PropManager.getLibName());
        height.addTextNode(book.getAdditional().getSize().getHeight().toString());
        SOAPElement width = size.addChildElement(WIDTH, PropManager.getLibName());
        width.addTextNode(book.getAdditional().getSize().getWidth().toString());
        SOAPElement length = size.addChildElement(LENGTH, PropManager.getLibName());
        length.addTextNode(book.getAdditional().getSize().getLength().toString());

        SOAPElement publicationYear = bookEl.addChildElement(PUBLICATION_YEAR, PropManager.getLibName());
        publicationYear.addTextNode(book.getPublicationYear().toString());
        return bookEl;
    }

    public static SOAPElement createGenre(SOAPElement element, Genre genre) throws SOAPException {
        SOAPElement genreEl = element.addChildElement(GENRE, PropManager.getLibName());
        SOAPElement genreId = genreEl.addChildElement(GENRE_ID, PropManager.getLibName());
        genreId.addTextNode(genre.getGenreId().toString());
        SOAPElement genreName = genreEl.addChildElement(GENRE_NAME, PropManager.getLibName());
        genreName.addTextNode(genre.getGenreName());
        SOAPElement genreDescription = genreEl.addChildElement(GENRE_DESCRIPTION, PropManager.getLibName());
        genreDescription.addTextNode(genre.getGenreDescription());
        return genreEl;
    }


    private Factory() {
    }
}
