package app.service;

import configurations.PropManager;
import factory.Factory;
import io.qameta.allure.Step;
import utils.SoapLogger;
import utils.entities.authors.Author;

import javax.xml.soap.*;

import static constants.SoapRequestElements.*;

public class AuthorService {
    @Step("Create author request")
    public SOAPMessage createAuthor(Author author) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();
        SOAPElement createAuthorRequest = soapBody.addChildElement(CREATE_AUTHOR_REQUEST, PropManager.getLibName());
        Factory.createAuthor(createAuthorRequest, author);
        soapRequest.saveChanges();
        SoapLogger.logRequest(soapRequest, CREATE_AUTHOR_REQUEST);
        SOAPMessage soapResponse = soapConnection.call(soapRequest, URL);
        SoapLogger.logResponse(soapResponse);
        soapConnection.close();
        return soapResponse;
    }

    @Step("Get author request")
    public SOAPMessage getAuthor(Long authId) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement getAuthorRequest = soapBody.addChildElement(GET_AUTHOR_REQUEST, PropManager.getLibName());
        SOAPElement authorId = getAuthorRequest.addChildElement(AUTHOR_ID, PropManager.getLibName());
        authorId.addTextNode(authId.toString());
        soapRequest.saveChanges();
        SoapLogger.logRequest(soapRequest, GET_AUTHOR_REQUEST);

        SOAPMessage soapResponse = soapConnection.call(soapRequest, URL);
        SoapLogger.logResponse(soapResponse);
        soapConnection.close();
        return soapResponse;
    }

    @Step("Delete author request")
    public SOAPMessage deleteAuthor(Long authId) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement deleteAuthorRequest = soapBody.addChildElement(DELETE_AUTHOR_REQUEST, PropManager.getLibName());
        SOAPElement authorId = deleteAuthorRequest.addChildElement(AUTHOR_ID, PropManager.getLibName());
        authorId.addTextNode(authId.toString());

        SOAPElement options = deleteAuthorRequest.addChildElement(OPTIONS, PropManager.getLibName());
        SOAPElement forcibly = options.addChildElement(FORCIBLY, PropManager.getLibName());
        forcibly.addTextNode("false");
        soapRequest.saveChanges();
        SoapLogger.logRequest(soapRequest, DELETE_AUTHOR_REQUEST);

        SOAPMessage soapResponse = soapConnection.call(soapRequest, URL);
        soapConnection.close();
        return soapResponse;
    }

    @Step("Update author request")
    public SOAPMessage updateAuthor(Author author) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();
        SOAPElement updateAuthorRequest = soapBody.addChildElement(UPDATE_AUTHOR_REQUEST, PropManager.getLibName());

        Factory.createAuthor(updateAuthorRequest, author);
        soapRequest.saveChanges();
        SoapLogger.logRequest(soapRequest, UPDATE_AUTHOR_REQUEST);
        SOAPMessage soapResponse = soapConnection.call(soapRequest, URL);
        soapConnection.close();
        return soapResponse;
    }
}
