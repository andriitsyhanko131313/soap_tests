package app.service;

import configurations.PropManager;
import factory.Factory;
import io.qameta.allure.Step;
import utils.entities.genres.Genre;

import javax.xml.soap.*;

import static constants.SoapRequestElements.*;

public class GenreService {
    @Step("Create genre request")
    public SOAPMessage createGenre(Genre genre) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement createGenreRequest = soapBody.addChildElement("createGenreRequest", PropManager.getLibName());
        Factory.createGenre(createGenreRequest, genre);

        soapRequest.saveChanges();
        SOAPMessage soapResponse = soapConnection.call(soapRequest, "http://localhost:8080/ws");
        soapConnection.close();
        return soapResponse;
    }

    @Step("Delete genre request")
    public SOAPMessage deleteGenre(Long id) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement deleteGenreRequest = soapBody.addChildElement("deleteGenreRequest", PropManager.getLibName());

        SOAPElement genreId = deleteGenreRequest.addChildElement(GENRE_ID, PropManager.getLibName());
        genreId.addTextNode(id.toString());

        SOAPElement options = deleteGenreRequest.addChildElement(OPTIONS, PropManager.getLibName());
        SOAPElement forcibly = options.addChildElement(FORCIBLY, PropManager.getLibName());
        forcibly.addTextNode("false");

        SOAPMessage soapResponse = soapConnection.call(soapRequest, "http://localhost:8080/ws");
        soapConnection.close();
        return soapResponse;
    }
}
