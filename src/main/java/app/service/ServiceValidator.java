package app.service;

import io.qameta.allure.Step;
import org.testng.Assert;
import utils.entities.authors.Author;
import utils.ResponseParser;
import utils.entities.books.Book;
import utils.entities.genres.Genre;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;


public class ServiceValidator {
    public static final String EXPECTED_BUT_FOUND = "Expected [%s], but found [%s]";

    @Step("Verify are authors equal")
    public void assertCompareAuthors(SOAPMessage response, Author exp, String responseNameElement) throws SOAPException {
        Assert.assertFalse(response.getSOAPBody().hasFault());
        Author actual = ResponseParser.parseToAuthor(response, responseNameElement);
        Assert.assertEquals(actual, exp, String.format(EXPECTED_BUT_FOUND, actual, exp));

    }

    @Step("Verify is fault message correct")
    public void isFaultMessageCorrect(SOAPMessage response, String expected) throws SOAPException {
        Assert.assertTrue(response.getSOAPBody().hasFault());
        String actual = response.getSOAPBody().getFault().getFaultString();
        Assert.assertEquals(actual, expected, String.format(EXPECTED_BUT_FOUND, actual, expected));
    }

    @Step("Verify status")
    public void assertCompareStatus(SOAPMessage response, String expected, String responseNameElement) throws SOAPException {
        Assert.assertFalse(response.getSOAPBody().hasFault());
        String actual = ResponseParser.parseToStatus(response, responseNameElement);
        Assert.assertEquals(actual, expected, String.format(EXPECTED_BUT_FOUND, actual, expected));
    }

    @Step("Verify are books equal")
    public void assertCompareBooks(SOAPMessage response, Book exp, String responseNameElement) throws SOAPException {
        Assert.assertFalse(response.getSOAPBody().hasFault());
        Book actual = ResponseParser.parseToBook(response, responseNameElement);
        Assert.assertEquals(actual, exp, String.format(EXPECTED_BUT_FOUND, actual, exp));
    }

    @Step("Verify are genres equal")
    public void assertCompareGenres(SOAPMessage response, Genre exp, String responseNameElement) throws SOAPException {
        Assert.assertFalse(response.getSOAPBody().hasFault());
        Genre actual = ResponseParser.parseToGenre(response, responseNameElement);
        Assert.assertEquals(actual, exp, String.format(EXPECTED_BUT_FOUND, actual, exp));
    }

    @Step("Verify has response fault")
    public void assertResponseHasFault(SOAPMessage response) throws SOAPException {
        Assert.assertTrue(response.getSOAPBody().hasFault());
    }
}
