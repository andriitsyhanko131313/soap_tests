package app.service;

import configurations.PropManager;
import factory.Factory;
import io.qameta.allure.Step;
import utils.SoapLogger;
import utils.entities.authors.Author;
import utils.entities.books.Book;
import utils.entities.genres.Genre;

import javax.xml.soap.*;


import static constants.SoapRequestElements.*;


public class BookService {
    @Step("Create book request")
    public SOAPMessage createBook(Author author, Book book, Genre genre) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement createBookRequest = soapBody.addChildElement(CREATE_BOOK_REQUEST, PropManager.getLibName());
        SOAPElement authorId = createBookRequest.addChildElement(AUTHOR_ID, PropManager.getLibName());
        authorId.addTextNode(author.getAuthorId().toString());

        SOAPElement genreId = createBookRequest.addChildElement(GENRE_ID, PropManager.getLibName());
        genreId.addTextNode(genre.getGenreId().toString());

        Factory.createBook(createBookRequest, book);

        soapRequest.saveChanges();
        SoapLogger.logRequest(soapRequest, CREATE_BOOK_REQUEST);
        SOAPMessage soapResponse = soapConnection.call(soapRequest, "http://localhost:8080/ws");
        soapConnection.close();
        return soapResponse;
    }

    @Step("Delete book request")
    public SOAPMessage deleteBook(Long bookId) throws SOAPException {
        SOAPConnection soapConnection = Factory.createConnection();
        SOAPMessage soapRequest = Factory.createSoapMessage();
        SOAPPart soapPart = soapRequest.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
        soapEnvelope.addNamespaceDeclaration(PropManager.getLibName(), PropManager.getLibNameURL());
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement deleteBookRequest = soapBody.addChildElement("deleteBookRequest", PropManager.getLibName());
        SOAPElement bookIdEl = deleteBookRequest.addChildElement(BOOK_ID, PropManager.getLibName());
        bookIdEl.addTextNode(bookId.toString());

        soapRequest.saveChanges();
        SOAPMessage soapResponse = soapConnection.call(soapRequest, "http://localhost:8080/ws");
        soapConnection.close();
        return soapResponse;
    }
}
