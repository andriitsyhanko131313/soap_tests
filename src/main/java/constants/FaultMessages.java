package constants;

public class FaultMessages {
    private FaultMessages() {
    }

    public static final String AUTHOR_EXIST_MESSAGE = "Author with id 10 already exists";
    public static final String STATUS_SUCCESSFULLY_DELETED_AUTHOR = "Successfully deleted author 10";
    public static final String WHEN_EMPTY_DATA_MESSAGE = "Text '' could not be parsed at index 0";
    public static final String STATUS_SUCCESSFULLY_DELETED_GENRE = "Successfully deleted genre 10";
    public static final String STATUS_SUCCESSFULLY_DELETED_BOOK = "Successfully deleted book 10";
}
