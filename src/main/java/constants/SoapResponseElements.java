package constants;

public class SoapResponseElements {
    private SoapResponseElements() {
    }

    public static final String CREATE_AUTHOR_RESPONSE = "ns2:createAuthorResponse";
    public static final String AUTHOR = "ns2:author";
    public static final String GET_AUTHOR_RESPONSE = "ns2:getAuthorResponse";
    public static final String DELETE_AUTHOR_RESPONSE = "ns2:deleteAuthorResponse";
    public static final String STATUS = "ns2:status";
    public static final String UPDATE_AUTHOR_RESPONSE = "ns2:updateAuthorResponse";
    public static final String CREATE_BOOK_RESPONSE = "ns2:createBookResponse";
    public static final String BOOK = "ns2:book";
    public static final String GENRE = "ns2:genre";
    public static final String CREATE_GENRE_RESPONSE = "ns2:createGenreResponse";
    public static final String DELETE_GENRE_RESPONSE = "ns2:deleteGenreResponse";
    public static final String DELETE_BOOK_RESPONSE = "ns2:deleteBookResponse";

}
