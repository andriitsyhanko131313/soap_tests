package constants;

public class SoapRequestElements {
    private SoapRequestElements() {
    }

    public static final String URL = "http://localhost:8080/ws";
    public static final String AUTHOR_ID = "authorId";
    public static final String GENRE_ID = "genreId";
    public static final String BOOK = "book";
    public static final String BOOK_ID = "bookId";
    public static final String BOOK_NAME = "bookName";
    public static final String BOOK_DESCRIPTION = "bookDescription";
    public static final String PAGE_COUNT = "pageCount";
    public static final String SIZE = "size";
    public static final String HEIGHT = "height";
    public static final String WIDTH = "width";
    public static final String LENGTH = "length";
    public static final String PUBLICATION_YEAR = "publicationYear";
    public static final String GENRE_NAME = "genreName";
    public static final String GENRE_DESCRIPTION = "genreDescription";
    public static final String BOOK_LANGUAGE = "bookLanguage";
    public static final String ADDITIONAL = "additional";
    public static final String GENRE = "genre";
    public static final String OPTIONS = "options";
    public static final String FORCIBLY = "forcibly";
    public static final String CREATE_BOOK_REQUEST = "createBookRequest";
    public static final String CREATE_AUTHOR_REQUEST = "createAuthorRequest";
    public static final String GET_AUTHOR_REQUEST = "getAuthorRequest";
    public static final String DELETE_AUTHOR_REQUEST = "deleteAuthorRequest";
    public static final String UPDATE_AUTHOR_REQUEST = "updateAuthorRequest";
}

