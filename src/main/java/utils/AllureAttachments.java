package utils;

import io.qameta.allure.Allure;

public class AllureAttachments {
    private AllureAttachments() {
    }

    public static void addFileToAllure(String fileName, String content) {
        Allure.addAttachment(fileName, content);
    }
}
