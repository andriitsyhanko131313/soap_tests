package utils.entities.genres;

import java.util.Objects;

public class Genre {
    private Long genreId;
    private String genreName;
    private String genreDescription;

    public Genre() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;
        Genre genre = (Genre) o;
        return getGenreId().equals(genre.getGenreId()) && getGenreName().equals(genre.getGenreName()) && getGenreDescription().equals(genre.getGenreDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGenreId(), getGenreName(), getGenreDescription());
    }

    @Override
    public String toString() {
        return "Genre{" +
                "genreId=" + genreId +
                ", genreName='" + genreName + '\'' +
                ", genreDescription='" + genreDescription + '\'' +
                '}';
    }

    public Long getGenreId() {
        return genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public String getGenreDescription() {
        return genreDescription;
    }

    public Genre(Long genreId, String genreName, String genreDescription) {
        this.genreId = genreId;
        this.genreName = genreName;
        this.genreDescription = genreDescription;
    }
}
