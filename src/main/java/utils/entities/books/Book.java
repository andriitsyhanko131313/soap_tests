package utils.entities.books;

import java.util.Objects;

public class Book {
    private Long bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;
    private Additional additional;
    private Integer publicationYear;

    public Book() {
    }

    public Book(Long bookId, String bookName, String bookLanguage, String bookDescription, Additional additional, Integer publicationYear) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookLanguage = bookLanguage;
        this.bookDescription = bookDescription;
        this.additional = additional;
        this.publicationYear = publicationYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getBookId().equals(book.getBookId()) && getBookName().equals(book.getBookName()) && getBookLanguage().equals(book.getBookLanguage()) && getBookDescription().equals(book.getBookDescription()) && getAdditional().equals(book.getAdditional()) && getPublicationYear().equals(book.getPublicationYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBookId(), getBookName(), getBookLanguage(), getBookDescription(), getAdditional(), getPublicationYear());
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", bookLanguage='" + bookLanguage + '\'' +
                ", bookDescription='" + bookDescription + '\'' +
                ", additional=" + additional +
                ", publicationYear=" + publicationYear +
                '}';
    }

    public Long getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public String getBookLanguage() {
        return bookLanguage;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public Additional getAdditional() {
        return additional;
    }

    public Integer getPublicationYear() {
        return publicationYear;
    }
}
