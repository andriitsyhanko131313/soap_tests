package utils.entities.books;

import java.util.Objects;

public class Additional {
    private Integer pageCount;
    private Size size;

    public Additional() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Additional)) return false;
        Additional that = (Additional) o;
        return getPageCount().equals(that.getPageCount()) && getSize().equals(that.getSize());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPageCount(), getSize());
    }

    @Override
    public String toString() {
        return "Additional{" +
                "pageCount=" + pageCount +
                ", size=" + size +
                '}';
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public Size getSize() {
        return size;
    }

    public Additional(Integer pageCount, Size size) {
        this.pageCount = pageCount;
        this.size = size;
    }
}
