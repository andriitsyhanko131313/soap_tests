package utils.entities.books;

import java.util.Objects;

public class Size {
    private Integer height;
    private Integer width;
    private Integer length;

    public Size() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Size)) return false;
        Size size = (Size) o;
        return getHeight().equals(size.getHeight()) && getWidth().equals(size.getWidth()) && getLength().equals(size.getLength());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHeight(), getWidth(), getLength());
    }

    @Override
    public String toString() {
        return "Size{" +
                "height=" + height +
                ", width=" + width +
                ", length=" + length +
                '}';
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getLength() {
        return length;
    }

    public Size(Integer height, Integer width, Integer length) {
        this.height = height;
        this.width = width;
        this.length = length;
    }
}
