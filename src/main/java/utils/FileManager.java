package utils;

import configurations.PropManager;
import utils.entities.authors.Author;
import utils.entities.books.Book;
import utils.entities.genres.Genre;

import java.util.List;

public class FileManager {
    private FileManager() {
    }
    public static Author getValidAuthor() {
        return FileReader.readObject(PropManager.getValidAuthorFilePath(), Author.class);
    }

    public static Author getAuthorUpdated() {
        return FileReader.readObject(PropManager.getAuthorUpdatedFilePath(), Author.class);
    }

    public static Book getValidBook() {
        return FileReader.readObject(PropManager.getValidBookFilePath(), Book.class);
    }

    public static Genre getValidGenre() {
        return FileReader.readObject(PropManager.getValidGenreFilePath(), Genre.class);
    }

    public static List<Author> getInvalidAuthors() {
        return FileReader.readListOfObject(PropManager.getInvalidAuthorsFilePath(), Author.class);
    }

    public static List<Author> getInvalidAuthorsBirthDate() {
        return FileReader.readListOfObject(PropManager.getInvalidAuthorBirthDateFilePath(), Author.class);
    }

}

