package configurations;

public class PropManager {
    private PropManager() {
    }

    public static String getLibName() {
        return PropertiesReader.readProperties("libName");
    }

    public static String getLibNameURL() {
        return PropertiesReader.readProperties("libNameURL");
    }

    public static String getAuthor1FilePath() {
        return PropertiesReader.readProperties("author1_file_path");
    }

    public static String getAuthor2FilePath() {
        return PropertiesReader.readProperties("author2_file_path");
    }

    public static String getValidAuthorFilePath() {
        return PropertiesReader.readProperties("valid_author_file_path");
    }

    public static String getAuthorUpdatedFilePath() {
        return PropertiesReader.readProperties("author_updated_file_path");
    }

    public static String getEmptyAuthorFilePath() {
        return PropertiesReader.readProperties("empty_author_file_path");
    }

    public static String getValidBookFilePath() {
        return PropertiesReader.readProperties("valid_book_file_path");
    }

    public static String getValidGenreFilePath() {
        return PropertiesReader.readProperties("valid_genre_file_path");
    }

    public static String getInvalidAuthorsFilePath() {
        return PropertiesReader.readProperties("invalid_authors_file_path");
    }

    public static String getInvalidAuthorBirthDateFilePath() {
        return PropertiesReader.readProperties("invalid_authors_birth_data_file_path");
    }
}

