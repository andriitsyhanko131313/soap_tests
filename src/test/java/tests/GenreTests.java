package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPException;


import static constants.SoapResponseElements.*;
import static constants.FaultMessages.*;

public class GenreTests extends TestData {

    @Test(description = "Create genre test case")
    public void createGenreTestCase() throws SOAPException {
        soapResponse = genreService.createGenre(validGenre);
        serviceValidator.assertCompareGenres(soapResponse, validGenre, CREATE_GENRE_RESPONSE);
    }

    @Test(description = "Delete genre test case")
    public void deleteGenreTestCase() throws SOAPException {
        soapResponse = genreService.createGenre(validGenre);
        serviceValidator.assertCompareGenres(soapResponse, validGenre, CREATE_GENRE_RESPONSE);
        soapResponse = genreService.deleteGenre(validGenre.getGenreId());
        serviceValidator.assertCompareStatus(soapResponse, STATUS_SUCCESSFULLY_DELETED_GENRE, DELETE_GENRE_RESPONSE);
    }

    @AfterMethod
    public void clearGenresDataTest() throws SOAPException {
        genreService.deleteGenre(validGenre.getGenreId());
    }
}
