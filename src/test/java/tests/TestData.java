package tests;

import app.service.AuthorService;
import app.service.BookService;
import app.service.GenreService;
import app.service.ServiceValidator;
import com.google.inject.Inject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Guice;
import utils.FileManager;
import utils.entities.authors.Author;
import utils.entities.books.Book;
import utils.entities.genres.Genre;

import javax.xml.soap.SOAPMessage;

@Guice
public class TestData {
    protected SOAPMessage soapResponse;
    @Inject
    protected AuthorService authorService;
    @Inject
    protected ServiceValidator serviceValidator;
    @Inject
    protected BookService bookService;
    @Inject
    protected GenreService genreService;
    protected Author validAuthor = FileManager.getValidAuthor();
    protected Author authorUpdated = FileManager.getAuthorUpdated();
    protected Book validBook = FileManager.getValidBook();
    protected Genre validGenre = FileManager.getValidGenre();

    @DataProvider
    protected Object[] invalidAuthors() {
        return FileManager.getInvalidAuthors().toArray();
    }

    @DataProvider
    protected Object[] invalidAuthorsBirthDate() {
        return FileManager.getInvalidAuthorsBirthDate().toArray();
    }
}
