package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import utils.entities.authors.Author;

import javax.xml.soap.*;

import static constants.FaultMessages.*;
import static constants.SoapResponseElements.*;


public class AuthorTests extends TestData {

    @Test(description = "Create author test case")
    public void createAuthorTestCase() throws SOAPException {
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, CREATE_AUTHOR_RESPONSE);
    }

    @Test(description = "Create author if already exist test case")
    public void createAuthorThatAlreadyExistTestCase() throws SOAPException {
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, CREATE_AUTHOR_RESPONSE);
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.isFaultMessageCorrect(soapResponse, AUTHOR_EXIST_MESSAGE);
    }

    @Test(description = "Get author test case")
    public void getAuthorTestCase() throws SOAPException {
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, CREATE_AUTHOR_RESPONSE);
        soapResponse = authorService.getAuthor(validAuthor.getAuthorId());
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, GET_AUTHOR_RESPONSE);
    }

    @Test(description = "Delete author test case")
    public void deleteAuthorTestCase() throws SOAPException {
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, CREATE_AUTHOR_RESPONSE);
        soapResponse = authorService.deleteAuthor(validAuthor.getAuthorId());
        serviceValidator.assertCompareStatus(soapResponse, STATUS_SUCCESSFULLY_DELETED_AUTHOR, DELETE_AUTHOR_RESPONSE);
    }

    @Test(description = "Update author test case")
    public void updateAuthorTestCase() throws SOAPException {
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, CREATE_AUTHOR_RESPONSE);
        soapResponse = authorService.updateAuthor(authorUpdated);
        serviceValidator.assertCompareAuthors(soapResponse, authorUpdated, UPDATE_AUTHOR_RESPONSE);
    }


    @Test(description = "Create author if invalid data test case",
            dataProvider = "invalidAuthors")
    public void createAuthorWithInvalidDataTestCase(Author invalidAuthor) throws SOAPException {
        soapResponse = authorService.createAuthor(invalidAuthor);
        serviceValidator.assertResponseHasFault(soapResponse);

        authorService.deleteAuthor(invalidAuthor.getAuthorId());
    }

    @Test(description = "Create author if invalid birth data",
            dataProvider = "invalidAuthorsBirthDate")
    public void createAuthorWithInvalidBirthData(Author invalidBirthDataAuthor) throws SOAPException {
        soapResponse = authorService.createAuthor(invalidBirthDataAuthor);
        serviceValidator.assertResponseHasFault(soapResponse);

        authorService.deleteAuthor(invalidBirthDataAuthor.getAuthorId());
    }

    @AfterMethod
    public void clearData() throws SOAPException {
        authorService.deleteAuthor(validAuthor.getAuthorId());
    }
}
