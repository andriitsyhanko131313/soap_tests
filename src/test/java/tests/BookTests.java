package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPException;


import static constants.SoapResponseElements.*;
import static constants.FaultMessages.*;

public class BookTests extends TestData {
    @Test(description = "Create book test case")
    public void createBookTestCase() throws SOAPException {
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, CREATE_AUTHOR_RESPONSE);
        soapResponse = genreService.createGenre(validGenre);
        serviceValidator.assertCompareGenres(soapResponse, validGenre, CREATE_GENRE_RESPONSE);
        soapResponse = bookService.createBook(validAuthor, validBook, validGenre);
        serviceValidator.assertCompareBooks(soapResponse, validBook, CREATE_BOOK_RESPONSE);
    }

    @Test(description = "Delete book test case")
    public void deleteBookTestCase() throws SOAPException {
        soapResponse = authorService.createAuthor(validAuthor);
        serviceValidator.assertCompareAuthors(soapResponse, validAuthor, CREATE_AUTHOR_RESPONSE);
        soapResponse = genreService.createGenre(validGenre);
        serviceValidator.assertCompareGenres(soapResponse, validGenre, CREATE_GENRE_RESPONSE);
        soapResponse = bookService.createBook(validAuthor, validBook, validGenre);
        serviceValidator.assertCompareBooks(soapResponse, validBook, CREATE_BOOK_RESPONSE);
        soapResponse = bookService.deleteBook(validBook.getBookId());
        serviceValidator.assertCompareStatus(soapResponse, STATUS_SUCCESSFULLY_DELETED_BOOK, DELETE_BOOK_RESPONSE);
    }

    @AfterMethod
    public void clearData() throws SOAPException {
        bookService.deleteBook(validBook.getBookId());
        genreService.deleteGenre(validGenre.getGenreId());
        authorService.deleteAuthor(validAuthor.getAuthorId());
    }

}
